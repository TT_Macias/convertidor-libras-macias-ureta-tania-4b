package facci.taniaMacias.convertidorLibras;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Conversion extends AppCompatActivity {
    TextView txtrp;

    int valor;
    double resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);

        txtrp = (TextView) findViewById(R.id.txtrp);
        //Bundle bundle= new Bundle();
        Bundle bundle = this.getIntent().getExtras();
        txtrp.setText(bundle.getString("dato"));

        valor= Integer.parseInt(bundle.getString("dato"));
        resultado =valor * 453.592;

        txtrp.setText(String.valueOf(resultado));




        Log.e("Valor", "Conversión Finalizada");
    }
}
